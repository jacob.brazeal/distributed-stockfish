# Deploy
You will need an AWS account to host the Lambda function backend. Prerequisites:
- AWS CLI set up locally (make sure you have permission to create lambda functions, log groups, etc)
- Node 14+

Then just run the following script in this folder:

```bash
bash setup.sh
```

It might take some time to run the first time (you can safely rerun it as many times as you want).
If everything works properly, the script will end with the following output:

```
Here is your API URL. You can run the following line to add the API URL as an environment variable:
export DISTRIBUTED_STOCKFISH_API_URL={URL} >> ~/.bashrc
```

You can run that line in your (bash) terminal to set up the environment variable. Alternatively,
you can copy the `{URL}` value for manual set up in the CLI - it will prompt you for the URL if it's
not found as an environment variable.
