const {analyze} = require("./analyse");
const err = (message, code = 400) => ({
    statusCode: code,
    body: JSON.stringify({"error": message}),
    headers: {
        'Content-Type': 'application/json'
    }
});

const ok = (result) => ({
    statusCode: 200,
    body: JSON.stringify(result),
    headers: {
        'Content-Type': 'application/json'
    }
});

exports.handler = async (event, context) => {
    const body = event.body ? JSON.parse(event.body) : {};
    if (!body.action) {
        return ok({
            "msg": "Everything is set up correctly."
        })
    }
    if (body.action === 'eval') {
        const {
            position,
            depth,
            multiPV,
            lookahead
        } = body;

        try {
            const result = await analyze(position, {depth, multiPV, lookahead})
            return ok(result);

        } catch (e) {
            return err('Hard failure processing position: ' + e.toString(), 500)
        }

    }
    return err("Unknown action: " + body.action);
};
