const stockfish = require('stockfish')();
const Chess = require('@ninjapixel/chess').Chess;

let _responsePattern = null, _responseCallback = null, _accumulatePattern = null, _responseID = 0, _responses = [];

stockfish.onmessage = function (event) {
    const response = event.data ? event.data : event;

    const shouldRespond = (_responsePattern && _responsePattern.test(response));
    const shouldAccumulate = (_accumulatePattern && _accumulatePattern.test(response))
    if (shouldAccumulate || shouldRespond) {
        _responses.push(response);
    }

    if (_responseCallback && shouldRespond) {
        _responseCallback([response, _responses]);
        // A one-time callback.
        _responsePattern = null;
        _accumulatePattern = null;
        _responseCallback = null;
        _responses = [];
    }

};

async function enginePromise(message, responsePattern = null, accumulatePattern = null) {
    let internalID = _responseID;
    return new Promise((res, rej) => {
        _responseID = internalID;
        _responsePattern = responsePattern;
        _accumulatePattern = accumulatePattern;
        _responseCallback = function (engineResponse) {
            if (_responseID !== internalID) return; // From another invocation
            res(engineResponse);
        }
        stockfish.postMessage(message);
    });
}

function engineMessage(message) {
    stockfish.postMessage(message);
}

async function sleep(ms) {
    return new Promise(res => setTimeout(res, ms));
}

async function awaitReady() {
    while (true) {
        const [status] = await enginePromise('isready', /^readyok$/);
        if (status === 'readyok') return true;
        await sleep(10);
    }
}

async function newPgn(pv = 1) {
    engineMessage(`setoption name MultiPV value ${pv}`)
    engineMessage('ucinewgame');
    engineMessage('position startpos');
    await awaitReady();
}

exports.analyze = async function (fen, options) {
    options = Object.assign({
        depth: 12,
        lookahead: 0,
        multiPV: 4
    }, options);
    const effectiveDepth = options.depth + options.lookahead;

    const chessObj = new Chess();

    await newPgn(options.multiPV);

    function extractCPFromInfoLine(lastInfoLine, isWhiteTurn = true) {
        let mate = false;
        if (lastInfoLine.includes("mate")) mate = true; // For now, we'll punt on analyzing mating situations
        else if (!lastInfoLine.includes("cp")) {
            throw ("No centipawn evaluation?");
        }

        function mateToEval(mateScore) {
            if (mateScore >= 0) return 99999;
            return -99999;
        }

        let newCentipawnEvaluation = mate
            ? mateToEval(parseInt(lastInfoLine.match(/ mate (-?\d+)/)[1]))
            : parseInt(lastInfoLine.match(/ cp (-?\d+)/)[1]);
        if (!isWhiteTurn) newCentipawnEvaluation *= -1; // Adjust to intuitive score (correct for UCI weirdness)
        return newCentipawnEvaluation;
    }

    function extractPvNoFromInfoLine(infoLine) {
        const m = infoLine.match(/multipv (\d+) /);
        if (m) {
            return parseInt(m[1]);
        }
        return null;
    }

    chessObj.load(fen);
    const isWhiteTurn = chessObj.turn() === chessObj.WHITE;
    engineMessage('position fen ' + fen);
    const [result, infos] = await enginePromise('go depth ' + effectiveDepth, /^bestmove.*/, /^info depth.*/);

    const infosByPv = {};
    for (const info of infos) {
        const p = extractPvNoFromInfoLine(info);
        if (p !== null) {
            infosByPv[p] = infosByPv[p] || [];
            infosByPv[p].push(info);
        }
    }
    const variations = Object.entries(infosByPv).sort(([pvA,], [pvB,]) => pvA - pvB).map(([p, lines]) => {
        const lastLine = lines[lines.length - 1];
        const depthLine = lines[Math.min(lines.length - 1, options.depth - 1)];
        console.log(depthLine);
        const [finalFen, sequence] = fenAndPVToFen(fen, depthLine, options.depth - 1); // Use the depth line for fen and move sequence
        const eval = extractCPFromInfoLine(lastLine, isWhiteTurn); // Use very last line (depth + lookup) to better inform eval
        return {
            fen: finalFen,
            sequence,
            eval
        }
    })

    console.log(variations);

    return {fen, variations, isWhiteTurn}

}

function fenAndPVToFen(fen, lastInfoLine, limit = 99) {
    let pv;
    if (!lastInfoLine.includes(' pv ')) {
        pv = [];
    } else {
        pv = lastInfoLine.split(' pv ')[1].split(' bmc')[0].split(' ');
    }
    pv = pv.slice(0, limit);
    const chessObj = new Chess();
    chessObj.load(fen);

    for (const move of pv) {
        chessObj.move(move, {sloppy: 'true'});
    }

    return [chessObj.fen(), pv];

}

// Simple demo

if (require.main === module) {
    exports.analyze('rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1', {lookahead: 3});
}



