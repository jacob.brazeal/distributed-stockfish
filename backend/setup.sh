#!/usr/bin/env bash
# 0. Fix working directory
original_dir="$PWD"
cd "$(dirname "$0")" || exit 1

# 1. Make infra
echo "Setting up AWS stack..."
aws cloudformation deploy --template-file stack.yaml --stack-name distributed-stockfish --capabilities CAPABILITY_NAMED_IAM 1> /dev/null
# 1a. Get API URL
API_URL=$(aws cloudformation describe-stacks --stack-name distributed-stockfish --query "Stacks[0].Outputs[?OutputKey=='distributedStockfishEndpointUrl'].OutputValue" --no-paginate --output text)
LAMBDA_ARN=$(aws cloudformation describe-stacks --stack-name distributed-stockfish --query "Stacks[0].Outputs[?OutputKey=='distributedStockfishLambdaArn'].OutputValue" --no-paginate --output text)

# 2. Deploy Lambda codebase
echo "Deploying Lambda..."
DIR_NAME='lambda'
BUNDLE_NAME="bundle"

cd "$DIR_NAME" || exit 1
npm install
zip -r "$BUNDLE_NAME.zip" .

# Deploy
aws lambda update-function-code --function-name "$LAMBDA_ARN" \
  --zip-file "fileb://$(pwd)/$BUNDLE_NAME.zip" 1> /dev/null
echo 'Waiting for function to be ready...'
sleep 2

# Cleanup
rm "$BUNDLE_NAME.zip"
cd "$original_dir" || exit 1

echo 'Testing deployment...'
curl --request POST https://gm6rk61jn8.execute-api.us-east-1.amazonaws.com/call
echo ''

echo 'Here is your API URL. You can run the following line to add the API URL as an environment variable:'
echo "export DISTRIBUTED_STOCKFISH_API_URL=$API_URL >> ~/.bashrc"

