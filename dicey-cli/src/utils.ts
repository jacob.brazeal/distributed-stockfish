export const sleep = async (ms: number) => new Promise((res) => setTimeout(res, ms));

export function dieIfFalsey<T>(x?: T): T {
  if (!x) throw new Error();
  return x;
}

export function strEvalToNumber(e: string): number {
  const asNumber = parseFloat(e);
  if (!isNaN(asNumber)) return asNumber;
  if (e.toLowerCase().includes('m')) {
    const [sign, count] = e.toLowerCase().split('m');
    const numCount = parseInt(count);
    let mul = 1;
    if (sign === '-') mul = -1;
    return mul * (1000 + 1000 / numCount);
  }

  throw new Error('Eval not recognized: ' + e);
}

export function CmpStrEval(a: string, b: string) {
  return strEvalToNumber(a) < strEvalToNumber(b);
}

export async function withRetries<T>(callable: () => T, retries = 5): Promise<T> {
  try {
    return await callable();
  } catch (e) {
    console.log('Failure: ', retries);
    if (!retries) throw e;
    await sleep(2000);
    return await withRetries(callable, retries - 1);
  }
}
