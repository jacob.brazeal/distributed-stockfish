import * as yargs from 'yargs';
import Eval, { STARTPOS } from './eval';
import { getEndpoint, config } from './api';

const args: any = yargs
  .scriptName('dicey')
  .usage('$0 <cmd> [args]')
  .command('eval', 'Evaluate a FEN string', (yargs) => {
    return yargs
      .option('fen', {
        type: 'string',
        default: STARTPOS,
        alias: 'f',
        describe: 'Position to evaluate',
      })
      .option('depth', {
        alias: 'd',
        default: 54,
        type: 'number',
      })
      .option('chunkDepth', {
        alias: 'c',
        default: 13,
        type: 'number',
      })
      .option('parallelism', {
        alias: 'p',
        default: 5,
        type: 'number',
      })
      .option('branch', {
        alias: 'b',
        default: 3,
        type: 'number',
      })
      .option('lookahead', {
        alias: 'l',
        default: 6,
        type: 'number',
      })
      .option('url', {
        alias: 'u',
        default: '',
        type: 'string',
      })
      .option('quiet', {
        alias: 'q',
        default: false,
        type: 'boolean',
      });
  })
  .help().argv;

(async () => {
  const command = args['_'][0];
  if (command === 'eval') {
    // Setup API
    if (args.url) {
      config.DIST_STOCKFISH_API = args.url;
    } else if (!getEndpoint()) {
      console.log(
        'You need to configure the API: set the DIST_STOCKFISH_API environment variable or use the option `--url {{url}}\n' +
          'Hint: go to https://gitlab.com/jacob.brazeal/distributed-stockfish/-/blob/main/backend/README.md to deploy the backend service.',
      );
      process.exit(1);
    }
    if (args.chunkDepth % 2 === 0) {
      console.log(
        `Parity of chunk depth (${args.chunkDepth}) should be odd - fixing this for you by incrementing to ${
          args.chunkDepth + 1
        }`,
      );
      args.chunkDepth += 1;
    }

    // Run
    const e = new Eval({
      branch: args.branch,
      depth: args.depth,
      parallelism: args.parallelism,
      chunkDepth: args.chunkDepth,
      lookahead: args.lookahead,
      quiet: args.quiet,
    });
    const result = await e.evalAndMove(args.fen);
    console.log(result);
  } else {
    console.log('Distributed Stockfish CLI\nCopyright (C) 2021 Jacob Brazeal and contributers');
    console.log('Run with --help to see options');
    process.exit(1);
  }
})();
