import { strEvalToNumber } from '../utils';

test('Eval string to number conversions', () => {
  expect(strEvalToNumber('2.5')).toEqual(2.5);
  expect(strEvalToNumber('M1')).toBeGreaterThan(1000);
  expect(strEvalToNumber('-M5')).toBeLessThan(-1000);
});
