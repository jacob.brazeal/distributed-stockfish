import Chess from '@ninjapixel/chess';
import Eval from "./eval";
import {DefaultConfig} from "./defaults";

const chess = new Chess.Chess();

interface PlayOptions {
    whiteAI: Eval,
    blackAI: Eval
}

export const play = async (options: PlayOptions) => {
    options = Object.assign({
        whiteAI: new Eval(DefaultConfig),
        blackAI: new Eval(DefaultConfig)
    }, options);

    let curTurn = 0;
    let moveNum = 0;
    const ais = [options.whiteAI, options.blackAI];
    const times: [number[], number[]] = [[], []]
    while (!chess.gameOver() && moveNum < 25) {

        const start = new Date().getTime();
        const {evaluation, move} = await ais[curTurn].evalAndMove(chess.fen());
        const duration = new Date().getTime() - start;
        times[curTurn].push(duration);
        chess.move(move, {sloppy: true});
        console.log("Eval according to " + ["white", "black"][curTurn] + ": ", evaluation);

        curTurn = 1 - curTurn;
        if (curTurn === 0) {
            ++moveNum;
        }
        console.log(move);
        console.log(chess.ascii())
    }

    const result = chess.inDraw() ? '1/2-1/2' : curTurn === 1 ? '1-0' : '0-1';
    console.log(result);
    console.log(times);

}
(async () => {
    for (let i = 0; i < 10; ++i) {
        const args = {
            whiteAI: new Eval({depth: 18, lookahead: 0, chunkDepth: 15, branch: 1}), // Naive Stockfish, depth 18
            blackAI:
                new Eval({depth: 54, chunkDepth: 15, lookahead: 3, parallelism: 100, branch: 3}) // Triple 11s
        }
        await play(args)
    }
})()
