import { SubmitPosition } from './api';
import { strEvalToNumber, withRetries } from './utils';
import {DefaultConfig, EvalOpts} from "./defaults";

interface TreeNode {
  depth: number;
  state: null | {
    fen: string;
    path: string[];
    eval: string;
    isWhiteTurn: boolean;
  };
  size: number;
  children: TreeNode[];
}

export const STARTPOS = 'startpos';

export default class Eval {
  depth: number;
  parallelism: number;
  branch: number;
  chunkDepth: number;
  lookahead: number;
  quiet: boolean;
  constructor(opts: EvalOpts) {
    const { depth, parallelism, branch, chunkDepth, lookahead, quiet } = Object.assign({},DefaultConfig, opts);
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    this.parallelism = parallelism!;
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    this.depth = depth!;
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    this.branch = branch!;
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    this.chunkDepth = chunkDepth!;
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    this.lookahead = lookahead!;
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    this.quiet = quiet!;
    console.log(this);
  }

  async evalAndMove(position: string) {
    if ( position === STARTPOS) {
      position = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1';
    }
    // 1. Create tree skeleton
    const tree = this.makeTree();
    let grandTotal: number = tree.size;
    console.log('Connecting to cloud servers...');
    // 2. Asynchronous directed queue with max parallelism
    const queue: [TreeNode, string, string[]][] = [[tree, position, []]];
    let active = 0,
      total = 0;
    const popNode = async (): Promise<any> => {
      if (!queue.length) return;
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const [node, fen, path] = queue[0]!;
      queue.splice(0, 1);
      if (active > this.parallelism) return;
      active++;
      const adjustedChunkDepth = this.chunkDepth + 1; // Have to compute longer chunks to make up for overlapping variations
      const { variations, isWhiteTurn } = await withRetries(() =>
        SubmitPosition(fen, adjustedChunkDepth, this.branch, this.lookahead),
      );
      total++;
      active--;
      if (!variations.length) {
        grandTotal -= node.size - 1; // Account for the children we won't see
        return; // No continuation
      }
      node.state = {
        fen,
        path,
        eval: variations[0].eval, // Contract: first variation is the principal one
        isWhiteTurn,
      };
      node.children.length = Math.min(node.children.length, variations.length); // Trim down
      if (!this.quiet) {
        console.log(
          `Node ${total}/${grandTotal} (${
            Math.round((1000 * total) / grandTotal) / 10
          }%) | ${active} servers currently running...`,
          'depth ' + path.length,
          isWhiteTurn,
          variations.map((x) => x.eval),
        );
      }
      for (let i = 0; i < node.children.length; ++i) {
        const child = node.children[i];
        const variation = variations[i];
        const childPath = path.concat(variation.sequence);
        queue.push([child, variation.fen, childPath]);
      }
      if (queue.length) {
        const openSlots = Math.min(queue.length, this.parallelism - Math.min(this.parallelism, active));
        return Promise.all(new Array(openSlots).fill(0).map(async () => popNode()));
      }
    };
    await popNode();
    const [evaluation, pv] = this.minimax(tree);
    return {
      evaluation,
      pv,
      move: pv[0]
    }
  }

  minimax(root: TreeNode): [number, string[]] {
    if (!root.state) throw new Error('You should hydrate the tree first');
    const e = strEvalToNumber(root.state?.eval);
    if (Math.abs(e) > 1000) {
      return [e, root.state?.path]; // Mate, it's game over
    }
    const hydratedChildren = root.children.filter((c) => c.state);
    if (hydratedChildren.length) {
      const values = hydratedChildren.map((c) => this.minimax(c));
      values.sort(([a], [b]) => a - b); // For black, low to high is a good order
      if (root.state?.isWhiteTurn) values.reverse(); // For white, high to low is a good order
      return values[0];
    } else {
      return [e, root.state?.path];
    }
  }

  /**
   * makeTree: Builds a skeleton cg-tree that we can fill in with chunks later.
   * @param depth
   */
  makeTree(depth = 0): TreeNode {
    const node: TreeNode = {
      depth,
      state: null,
      children: [],
      size: 1,
    };
    if (depth + this.chunkDepth <= this.depth) {
      for (let i = 0; i < this.branch; ++i) {
        const child = this.makeTree(depth + this.chunkDepth);
        node.children.push(child);
        node.size += child.size;
      }
    }

    return node;
  }
}
