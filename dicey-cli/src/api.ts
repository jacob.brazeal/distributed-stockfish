import fetch from 'node-fetch';
import { dieIfFalsey } from './utils';

export const config: {
  DIST_STOCKFISH_API?: string;
} = {};

export const getEndpoint = () => {
  return config.DIST_STOCKFISH_API || process.env.DIST_STOCKFISH_API;
};

export interface PositionState {
  fen: string;
  sequence: string[];
  eval: string;
}

interface SubmitPositionResults {
  variations: PositionState[];
  fen: string;
  isWhiteTurn: boolean;
}

export const SubmitPosition = async (
  position: string,
  depth: number,
  multiPV: number,
  lookahead: number,
): Promise<SubmitPositionResults> => {
  const url: string = dieIfFalsey(getEndpoint());
  return (await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      action: 'eval',
      position,
      depth,
      multiPV,
      lookahead,
    }),
  })
    .then((x: any) => {
      if (x.status !== 200) {
        x.text().then(
            (y: string) => console.log(y));
        throw Error(x.status);
      }
      return x;
    })
    .then((x: any) => x.json())) as SubmitPositionResults;
};
