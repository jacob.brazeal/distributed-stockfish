import Eval from "./eval";

export interface EvalOpts {
    depth?: number;
    parallelism?: number;
    branch?: number;
    chunkDepth?: number;
    lookahead?: number;
    quiet?: boolean;
}


export const DefaultConfig: EvalOpts = {
    depth: 31,
    parallelism: 50,
    branch: 3,
    chunkDepth: 11,
    lookahead: 5,
    quiet: false
}
