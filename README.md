# Distributed Stockfish
This repository distributes Stockfish by allowing you to break the game tree into subtrees that
can be individually evaluated by AWS Lambda functions. The controller that coordinates chunking up
the game tree is a Typescript package in `dicey-cli/`. Code and a guide
for setting up the requisite AWS infrastructure
is in `backend/`.

This is a very simple idea, not even unique to chess, so what's the catch? Well, in chess engines the 
hash table the tracks visited positions is very important, particularly in endgames when you have a small
number of pieces that can easily reach the same position through different permutations. We don't maintain any
hash table between different invocations of Stockfish on different positions. However, in practice, there are usually
a very small number of isomorphic positions in the opening and midgame. So we adjust by intentionally restricting
the parallelism to 1 stockfish function at the beginning of the game and when there are very few pieces, as at the endgame.

# DICEY CLI
Before you get started, make sure that you have access to a backend deployment URL
(see [Backend README](../backend/README.md)). TL;DR only requires running a simple Bash script
if you have your own AWS account. You can either store that URL in an environment
variable as described on that page or pass it as a URL via the `--url` option on the CLI.

Now, ensure that you have Node JS 14+ installed.
Next, you can install this package from NPM with `npm i -g distributed-stockfish`.
Then you can run it like so (may require starting a new terminal session);
note the many optional parameters:
```bash
dicey eval [--fen "fenStr"] [--depth 54] [--chunkDepth 13] [--parallelism 5]
[--lookahead 6] [--branch 3] [--url backendAPIUrl] [--quiet]
```

A quick overview of the optional optional:

- fen: A FEN string for the position to evaluate. Defaults to the starting position.
- depth: The depth that you want to evaluate at. Dicey supports really big depths -- like 60+!
- chunkDepth: The depth of an individual chunk. This should be a reasonably large fraction of `depth`
- to reduce the total chunks you are processing. Also, the sum `chunkDepth + lookahead` should hover around
  15 - 20 for best accuracy without causing individual nodes to take a long time to process.
- parallelism: This lever goes allllll the way up! Technically depends on the Lambda concurrency
  settings on your account, but probably can support several hundred parallel processes without a problem.
  (But: watch that AWS bill)
- branch: How many principal variations to show, also the branch factor for each partial variation.
  As this goes up you may see better accuracy but also an exponential increase in nodes, so be careful!
- lookahead: Ideally around 5 + 6, helps with accuracy. Again, recommend keeping the sum `chunkDepth + lookahead`
  at around 15 - 20.
- quiet: See less output

Required:
- url: Your personal Dicey Backend URL, see [Backend README](../backend/README.md). You can
  also supply this as an environment variable via the instructions there.
